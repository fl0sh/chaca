// TOP

#include "errorcodes.h"

typedef u32 errorcode;

void
ReportError(errorcode Errorcode, const char* Message)
{
	switch (Errorcode)
	{
	case ERR_SDL_INIT_FAILED:
		fprintf(stderr, "\tSDL_Init failed\n");
		exit(Errorcode);
		break;
	case ERR_SDL_WINDOW_CREATION_FAILED:
		fprintf(stderr, "\tSDL_CreateWindow failed\n");
		exit(Errorcode);
		break;
	case ERR_SDL_GL_CONTEXT_CREATION_FAILED:
		fprintf(stderr, "\tSDL_GL_CreateContext failed\n");
		exit(Errorcode);
		break;
	case ERR_SHADER_COMPILATION_FAILED:
		fprintf(stderr, "\tShader Compilation failed with Message\n"
				"\t%s\n", Message);
		fprintf(stderr, "UNKNOWN BEHAVIOUR YET -> not terminating\n");

	default:
		break;
	}
}
// BOTTOM

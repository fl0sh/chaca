// TOP


struct vertexarray {
	u32 Id;
	u32 BufferCount;
};


void
CreateVertexArray(struct vertexarray *VertexArray)
{
	glGenVertexArrays(1, &(VertexArray->Id));
	VertexArray->BufferCount = 0;
}


void
BindVertexArray(struct vertexarray *VertexArray)
{
	glBindVertexArray(VertexArray->Id);
}


// BOTTOM

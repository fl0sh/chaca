// TOP


struct indexbuffer {
	unsigned int Id;
};


void
CreateIndexBuffer(struct indexbuffer *IndexBuffer,
		  u32 Count,
		  const u32 *Data)
{
	glGenBuffers(1, &(IndexBuffer->Id));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBuffer->Id);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, Count*sizeof(u32), Data, GL_STATIC_DRAW);
}


void
DeleteIndexBuffer(struct indexbuffer *IndexBuffer)
{
	glDeleteBuffers(1, &(IndexBuffer->Id));
}


void
BindIndexBuffer(struct indexbuffer *IndexBuffer)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBuffer->Id);
}


void
UnbindIndexBuffer()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

// BOTTOM

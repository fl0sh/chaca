// TOP


struct vertexbuffer {
	u32 Id;
};


void
CreateVertexBuffer(struct vertexbuffer *VertexBuffer,
		   u32 Size, const void *Data)
{
	glGenBuffers(1, &(VertexBuffer->Id));
	glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer->Id);
	glBufferData(GL_ARRAY_BUFFER, Size, Data, GL_STATIC_DRAW);
}


void
DeleteVertexBuffer(struct vertexbuffer *VertexBuffer)
{
	glDeleteBuffers(1, &(VertexBuffer->Id));
}


void
BindVertexBuffer(struct vertexbuffer *VertexBuffer)
{
	glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer->Id);
}


void
UnbindVertexBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


// BOTTOM

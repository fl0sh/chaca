// TOP


struct shader {
	u32 Id;
};


bool
CheckCompiledShader(u32 Shader)
{
	i32 ShaderCompiled;
	glGetShaderiv(Shader, GL_COMPILE_STATUS, &ShaderCompiled);
	if (ShaderCompiled == GL_FALSE)
	{
		int Length;
		glGetShaderiv(Shader, GL_INFO_LOG_LENGTH, &Length);

		char *Message = (char*)alloca(Length * sizeof(char));
		glGetShaderInfoLog(Shader, Length, &Length, Message);

		ReportError(ERR_SHADER_COMPILATION_FAILED, Message);

		glDeleteShader(Shader);

		return false;
	}
	return true;
}


std::string
ReadShaderFile(const std::string &Filepath)
{
	std::ifstream FileStream(Filepath);
	std::string Line;
	std::stringstream StringStream;

	while (getline(FileStream, Line))
	{
		StringStream << Line << '\n';
	}
	
	FileStream.close();

	return StringStream.str();
}


bool
MakeAndUseShader(struct shader *Shader)
{
	Shader->Id = glCreateProgram();

	std::string VertexShaderSource = ReadShaderFile("shader/default.vs");
	std::string FragmentShaderSource = ReadShaderFile("shader/default.fs");
	const char *VSSource = VertexShaderSource.c_str();
	const char *FSSource = FragmentShaderSource.c_str();
	u32 vsid = glCreateShader(GL_VERTEX_SHADER);
	u32 fsid = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(vsid, 1, &VSSource, 0);
	glShaderSource(fsid, 1, &FSSource, 0);
	glCompileShader(vsid);
	glCompileShader(fsid);
	if (CheckCompiledShader(vsid) == false || 
		CheckCompiledShader(fsid) == false)
	{
		return false;
	}
	glAttachShader(Shader->Id, vsid);
	glAttachShader(Shader->Id, fsid);
	glLinkProgram(Shader->Id);
	glValidateProgram(Shader->Id);
	glUseProgram(Shader->Id);

	return true;
}


// BOTTOM

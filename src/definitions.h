#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <stdint.h>

#define persist_var static
#define internal static
#define global_var static

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#endif /* DEFINITIONS_H */

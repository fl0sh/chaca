// TOP

global_var SDL_GLContext GlContext;

SDL_Window *
CreateWindow(const char *Name, i32 Width, i32 Height, i32 MinWidth, i32 MinHeight)
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		ReportError(ERR_SDL_INIT_FAILED, NULL);
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_Window *Window = SDL_CreateWindow(
			Name,
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			Width,
			Height,
			SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

	if (Window == NULL)
	{
		ReportError(ERR_SDL_WINDOW_CREATION_FAILED, NULL);
	}

	SDL_SetWindowMinimumSize(Window, MinWidth, MinHeight);
	
	GlContext = SDL_GL_CreateContext(Window);
	if (!GlContext)
	{
		ReportError(ERR_SDL_GL_CONTEXT_CREATION_FAILED, NULL);
	}

	//ReportError calls exit(Errorcode)
	return Window;
}

void
CloseWindow(SDL_Window *Window)
{
	SDL_GL_DeleteContext(GlContext);
	SDL_DestroyWindow(Window);
}


// BOTTOM

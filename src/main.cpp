// TOP

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include <SDL2/SDL.h>
#include <GL/gl.h>

#include "definitions.h"
#include "errors/errors.cpp"

#include "sdl/window.cpp"
#include "sdl/events.cpp"
#include "graphics/vertexarray.cpp"
#include "graphics/vertexbuffer.cpp"
#include "graphics/indexbuffer.cpp"
#include "graphics/shader.cpp"
#include "graphics/render.cpp"


int
main(void)
{
	SDL_Window *Window = CreateWindow("Chaca", 480, 360, 360, 300);
	printf("OpenGL Version: %s\n", glGetString(GL_VERSION));

	struct shader Shader;
	MakeAndUseShader(&Shader);
	
	struct vertexarray VertexArray;
	CreateVertexArray(&VertexArray);
	BindVertexArray(&VertexArray);

	struct vertexbuffer VertexBuffer;
	float Positions[] = {
		-0.5f, -0.5f,
		 0.5f, -0.5f,
		-0.5f,  0.5f,
		 0.5f,  0.5f
	};
	CreateVertexBuffer(&VertexBuffer, 8*sizeof(float), Positions);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float)*2 , 0);

	struct indexbuffer IndexBuffer;
	unsigned int Indices[] = {
		0, 1, 2,
		1, 2, 3
	};
	CreateIndexBuffer(&IndexBuffer, 6, Indices);

	SDL_Event Event;
	for (;;)
	{
		if (!HandleEvents(Window, &Event))
		{
			break;
		}

		Render(Window);
	}

	SDL_Quit();
	return(0);
}


// BOTTOM
